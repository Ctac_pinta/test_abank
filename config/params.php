<?php

return [
    'adminEmail' => 'admin@example.com',
    'upload_dir' => 'user_auth_file',
    'def_img' => '/image/fff.png',
    'terminal_status' => [
        1 => "на складе",
        2 => "транспортировка",
        3 => "установлен",
        4 => "активен",
        5 => "деактивирован",
    ]
];
