function PrintText() {
    var str = '';
    var number = parseInt(document.getElementById('number-field').value);
    var numbers4 = [
        ['один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
        ['десять', 'двадцать ', 'тридцать ', 'сорок ', 'пятьдесят ', 'шестьдесят ', 'семьдесят ', 'восемьдесят ', 'девяносто '],
        ['сто ', 'двести ', 'триста ', 'четыреста ', 'пятьсот ', 'шестьсот ', 'семьсот ', 'восемьсот ', 'девятьсот '],
        ['одна тысяча ', 'две тысячи ', 'три тысячи ', 'четыре тысячи ', 'пять тысячь ', 'шесть тысячь ', 'семь тысячь ', 'восемь тысячь ', 'девять тысячь '],
    ];

    if (number > 0 && number <= 9999) {
        number = number.toString();
        for (i = 0; i < number.length; i++) {
            if ((number[i] - 1) >= 0) {
                x = parseInt((number[i]) + '' + (number[i + 1]));
                if (x > 10 && x < 20 && number.length == 4 && i > 1 || x > 10 && x < 20 && number.length == 3 && i > 0 || x > 10 && x < 20 && number.length == 2) {
                    if(x==12){
                        str += 'двенадцать';
                    }else{
                        str += numbers4[0][number[i+1]-1] + 'надцать';
                    }
                    PrintToBlock(str);
                    return false;
                }

                str += numbers4[number.length - 1 - i][number[i] - 1];
                PrintToBlock(str);
            } else {
                PrintToBlock(str);
            }
        }
    }
}
function PrintToBlock(str) {
    document.getElementById("print-text").innerHTML = str;
}
$(document).ready(function () {
    $('#list').click(function (event) {
        event.preventDefault();
        $('#products .item').addClass('list-group-item');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#products .item').removeClass('list-group-item');
        $('#products .item').addClass('grid-group-item');
    });
});