<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * AddUserForm is the model behind the add add user to file form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegisterUserForm extends Model
{

    public $username;
    public $password;
    public $rememberMe = true;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            [['username'], 'validateUsername'],
        ];
    }

    /**
     * Validates the username.
     * This method serves as the inline validation for username
     *
     * @param $attribute
     * @param $params
     */
    public function validateUsername($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = Users::find()->where(['username' => $this->username])->one();
            if (!empty($user)) {
                $this->addError($attribute, 'Пользователь с таким именем зарегистрирован.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */

    public function register()
    {
        if (!$this->hasErrors()) {
            $user = new Users();
            $user->username = $this->username;
            $user->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $user->authKey = md5($this->username);
            $user->accessToken = md5($this->username);
            $user->loginTryCount = 0;
            $user->lastLogin = 0;
            if ($response = $user->save()) {
                Yii::$app->getSession()->setFlash('succes', 'Возникла проблема при авторизации');
            } else {
                Yii::$app->getSession()->setFlash('error', 'Возникла проблема регистрации пользователя');
            }

            return $response;
        }
    }
}
