<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employees".
 *
 * @property int $id
 * @property string $name
 *
 * @property Terminals[] $terminals
 * @property Subdivisions $subdivisions
 */
class Employees extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['subdivisions_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subdivisions::className(), 'targetAttribute' => ['subdivisions_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'subdivisions_id' => 'Отделение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerminals()
    {
        return $this->hasMany(Terminals::className(), ['employees_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubdivisions()
    {
        return $this->hasOne(Subdivisions::className(), ['id' => 'subdivisions_id']);
    }

    public function getSubdivisionsWithTerminalCount(){
        $result = Terminals::find()
            ->select(['s.name, COUNT(*) as c'])
            ->from('terminals t')
            ->join('LEFT JOIN','subdivisions s','t.subdivisions_id=s.id')
            ->where(['t.employees_id'=>$this->id])
            ->groupBy('t.subdivisions_id')
            ->asArray()
            ->all();
        return $result;
    }
}
