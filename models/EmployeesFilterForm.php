<?php

namespace app\models;

use Yii;
use yii\base\Model;
/**
 * This is the model class for Employees Form Filter.
 *
 */
class EmployeesFilterForm extends Model
{
    public $subdivisions_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subdivisions_id'], 'required'],
            [['subdivisions_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'subdivisions_id' => 'Подраздиление',
        ];
    }

}
