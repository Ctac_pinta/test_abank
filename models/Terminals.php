<?php

namespace app\models;

use Faker\Provider\ka_GE\DateTime;
use Yii;

/**
 * This is the model class for table "terminals".
 *
 * @property int $id
 * @property int $subdivisions_id
 * @property int $employees_id
 * @property string $code
 * @property string $image
 * @property string $manufacture
 * @property int $installation_date
 * @property int $last_service_date
 * @property int $status
 *
 * @property Employees $employees
 * @property Subdivisions $subdivisions
 */
class Terminals extends \yii\db\ActiveRecord
{
    const DEFAULT_STATUS = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'terminals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subdivisions_id', 'employees_id', 'status'], 'integer'],
            [['installation_date', 'last_service_date'], 'safe'],
            [['code'], 'required'],
            [['code', 'image', 'manufacture'], 'string', 'max' => 255],
            [['code'], 'unique'],
            [['employees_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employees::className(), 'targetAttribute' => ['employees_id' => 'id']],
            [['subdivisions_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subdivisions::className(), 'targetAttribute' => ['subdivisions_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subdivisions_id' => 'Подрозделение',
            'employees_id' => 'Ответственый сотрудник',
            'code' => 'Код',
            'image' => 'Картинка',
            'manufacture' => 'Производитель',
            'installation_date' => 'Дата установки',
            'last_service_date' => 'Дата посленего обслуживания',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployees()
    {
        return $this->hasOne(Employees::className(), ['id' => 'employees_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubdivisions()
    {
        return $this->hasOne(Subdivisions::className(), ['id' => 'subdivisions_id']);
    }

    /**
     * convert date to timestamp before save
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (\DateTime::createFromFormat('d-m-Y', $this->installation_date) !== FALSE) {
            $date = new \DateTime($this->installation_date, new \DateTimeZone('utc'));
            $this->installation_date = $date->getTimestamp();
        }
        if (\DateTime::createFromFormat('d-m-Y', $this->last_service_date) !== FALSE) {
            $date = new \DateTime($this->last_service_date, new \DateTimeZone('utc'));
            $this->last_service_date = $date->getTimestamp();
        }
        return parent::beforeSave($insert);
    }

    /**
     * convert timestamp to date after select
     */
    public function afterFind(){
        if(!empty($this->installation_date)){
            $date = new \DateTime();
            $date->setTimestamp($this->installation_date);
            //$date->setTimezone(new \DateTimeZone('utc'));
            $this->installation_date = $date->format('d-m-Y');
        }
        if(!empty($this->last_service_date)){
            $date = new \DateTime();
            $date->setTimestamp($this->last_service_date);
            //$date->setTimezone(new \DateTimeZone('utc'));
            $this->last_service_date = $date->format('d-m-Y');
        }
    }

}
