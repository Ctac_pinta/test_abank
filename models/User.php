<?php

namespace app\models;

use Yii;
use app\models\Users;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $loginTryCount;
    public $lastLogin;

    public function __construct(Users $user)
    {
        $this->id = $user->id;
        $this->username = $user->username;
        $this->password = $user->password;
        $this->authKey = $user->authKey;
        $this->accessToken = $user->accessToken;
        $this->loginTryCount = $user->loginTryCount;
        $this->lastLogin = $user->lastLogin;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        if ($user = Users::find()->where(['id'=>$id])->one()) {
            return new static($user);
        }
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        var_dump("token");
        die();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
