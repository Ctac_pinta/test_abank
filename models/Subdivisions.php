<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subdivisions".
 *
 * @property int $id
 * @property string $name
 * @property string $city
 *
 * @property Terminals[] $terminals
 */
class Subdivisions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subdivisions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'city'], 'required'],
            [['name'], 'string', 'max' => 4],
            [['city'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'city' => 'Город',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerminals()
    {
        return $this->hasMany(Terminals::className(), ['subdivisions_id' => 'id']);
    }
}
