<?php

use yii\db\Migration;

/**
 * create tables
 * Class m180829_073521_create_database
 *
 */
class m180829_073521_create_database extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        /**
         * start create user table
         */
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string(255)->notNull()->unique(),
            'password' => $this->text(),
            'authKey' => $this->string(255),
            'accessToken' => $this->string(255),
            'loginTryCount' => $this->integer()->defaultValue(0),
            'lastLogin' => $this->integer()->defaultValue(0)
        ]);

        $this->createIndex(
            'users_idx',
            'users',
            'id'
        );
        $this->createIndex(
            'users_username_idx',
            'users',
            'username'
        );
        /**
         * end create user table
         */

        /**
         * start create terminals table
         */
        $this->createTable('terminals', [
            'id' => $this->primaryKey(),
            'subdivisions_id' => $this->integer(),
            'employees_id' => $this->integer(),
            'code' => $this->string(255)->notNull()->unique(),
            'image' => $this->string(255),
            'manufacture' => $this->string(255),
            'installation_date' => $this->integer(),
            'last_service_date' => $this->integer(),
            'status' => $this->integer(1)->defaultValue(1)->notNull(),
        ]);

        $this->createIndex(
            'terminals_idx',
            'terminals',
            'id'
        );
        $this->createIndex(
            'terminals_code_idx',
            'terminals',
            'code'
        );
        $this->createIndex(
            'terminals_subdivisions_idx',
            'terminals',
            'subdivisions_id'
        );
        $this->createIndex(
            'terminals_employees_idx',
            'terminals',
            'employees_id'
        );
        /**
         * end create terminals table
         */

        /**
         * start create subdivisions table
         */
        $this->createTable('subdivisions', [
            'id' => $this->primaryKey(),
            'name' => $this->string(4)->notNull(),
            'city' => $this->string(255)->notNull(),
        ]);

        $this->createIndex(
            'subdivisions_idx',
            'subdivisions',
            'id'
        );
        $this->createIndex(
            'subdivisions_name_idx',
            'subdivisions',
            'name'
        );
        /**
         * end create subdivisions table
         */


        /**
         * start create regional_centers table
         */
        $this->createTable('regional_centers', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->createIndex(
            'regional_centers_idx',
            'regional_centers',
            'id'
        );
        $this->createIndex(
            'regional_centers_name_idx',
            'regional_centers',
            'name'
        );
        /**
         * end create regional_centers table
         */

        /**
         * start create employees table
         */
        $this->createTable('employees', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'subdivisions_id' => $this->integer(),
        ]);

        $this->createIndex(
            'employees_idx',
            'employees',
            'id'
        );
        $this->createIndex(
            'employees_name_idx',
            'employees',
            'name'
        );
        $this->createIndex(
            'employees_subdivisions_idx',
            'employees',
            'subdivisions_id'
        );
        /**
         * end create employees table
         */

        $this->addForeignKey(
            'fk_terminals_subdivisions_id',
            'terminals',
            'subdivisions_id',
            'subdivisions',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk_terminals_employees_id',
            'terminals',
            'employees_id',
            'employees',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        /**
         * drop  foreign key
         */
        $this->dropForeignKey('fk_terminals_subdivisions_id','terminals');
        $this->dropForeignKey('fk_terminals_employees_id','terminals');
        /**
         * drop  tables
         */
        $this->dropTable('users');
        $this->dropTable('terminals');
        $this->dropTable('subdivisions');
        $this->dropTable('regional_centers');
        $this->dropTable('employees');

    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180829_073521_create_database cannot be reverted.\n";

        return false;
    }
    */
}
