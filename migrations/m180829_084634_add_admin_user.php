<?php

use yii\db\Migration;

/**
 * Class m180829_084634_add_admin_user
 */
class m180829_084634_add_admin_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $user = new \app\models\Users();
        $user->username = 'admin';
        $user->password = Yii::$app->getSecurity()->generatePasswordHash('admin');
        $user->authKey = md5('admin');
        $user->accessToken = md5('admin');
        $user->loginTryCount = 0;
        $user->lastLogin = 0;
        $user->save(false);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        /**
         * @var \app\models\Users() $user
         */

        if(!empty($user = \app\models\Users::find()->where(['username'=>'admin'])->one())){
            $user->delete();
        }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180829_084634_add_admin_user cannot be reverted.\n";

        return false;
    }
    */
}
