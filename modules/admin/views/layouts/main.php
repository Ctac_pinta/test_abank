<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse',
        ],
    ]);
    if(Yii::$app->controller->id=='test-four'){
        $links=(Yii::$app->user->isGuest)? [
            ['label' => 'Login', 'url' => ['/site/login']],
            ['label' => 'Register', 'url' => ['/site/register']],
        ]:[
            ['label' => Yii::$app->user->identity->username, 'url' => ['/test-four/success-login']],
            ['label' => 'Logout', 'url' => ['/site/logout']],
        ];
        echo Nav::widget([
            'items' => $links,
            'options' => ['class' => 'navbar-nav pull-right'],
        ]);
    }


    NavBar::end();
    ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <?= $this->render("menu"); ?>
            </div>
            <div class="col-md-9">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>

    </div>
</div>

<footer class="footer">
    <div class="container">

    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
