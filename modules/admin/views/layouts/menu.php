<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;


echo Nav::widget([
    'items' => [
        [
            'label' => 'Терминалы',
            'url' => ['/admin'],
            'active' => stripos($this->context->route, 'admin/default') === false ? false : true
        ],
        [
            'label' => 'Работники',
            'url' => ['/admin/employees'],
            'active' => stripos($this->context->route, 'admin/employees') === false ? false : true
        ],
        [
            'label' => 'Подразделения',
            'url' => ['/admin/subdivisions'],
            'active' => stripos($this->context->route, 'admin/subdivisions') === false ? false : true
        ],
    ],
    'options' => ['class' => 'admin-menu'],
]);


?>

