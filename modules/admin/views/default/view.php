<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Terminals */

$this->title = $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Терминалы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="terminals-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'subdivisions_id' => [
                'label' => 'Подразделение',
                'value' => $model->subdivisions->name . ' ' . $model->subdivisions->city,
            ],
            'employees_id' => [
                'label' => 'Ответственый сотрудник',
                'value' => $model->employees->name . ' ' . $model->employees->subdivisions->name . ' ' . $model->employees->subdivisions->city,
            ],
            'code',
            'image',
            'manufacture',
            'installation_date',
            'last_service_date',
            'status'=>[
                'label' => 'Статус',
                'value' => isset(Yii::$app->params['terminal_status'][$model->status])?Yii::$app->params['terminal_status'][$model->status]:'',
            ],
        ],
    ]) ?>

</div>
