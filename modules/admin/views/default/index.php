<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use \yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\TerminalsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Терминалы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="terminals-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать терминал', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'image'=>[
                'label' => 'Изображение',
                'filter' => false,
                'format' => 'html',
                'value' =>function ($model) {
                    return !empty($model->image)?'<img src="'.$model->image.'" height="50" width="50">':'';
                },
            ],
            'subdivisions_id' => [
                'attribute' => 'subdivisions_id',
                'label' => 'Подраздиление',
                'value' => function ($model) {
                    return $model->subdivisions->name . ' ' . $model->subdivisions->city;
                },
                'filter' => Select2::widget([
                    'name' => 'ObjectSearch[subdivisions_id]',
                    'initValueText' => $searchModel->subdivisions->name . ' ' . $searchModel->subdivisions->city, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => '/admin/subdivisions/search-subdivisions',
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),

                    ]
                ]),
            ],
            'employees_id' => [
                'label' => 'Ответственый сотрудник',
                'attribute' => 'employees_id',
                'value' =>function ($model) {
                    return $model->employees->name . ' ' . $model->employees->name . ' ' . $model->employees->subdivisions->city;
                },
                'filter' => Select2::widget([
                    'name' => 'ObjectSearch[employees_id]',
                    'initValueText' => $searchModel->employees->name . ' ' . $searchModel->employees->subdivisions->city, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => '/admin/employees/search-employees',
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ]
                ]),
            ],
            'code',
            'manufacture',
            'installation_date',
            'last_service_date',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
