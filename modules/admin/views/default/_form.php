<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use kartik\date\DatePicker;
use app\modules\admin\models\Terminals;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Terminals */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="terminals-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subdivisions_id')->widget(Select2::classname(), [
        'initValueText' => $model->subdivisions->name . ' ' . $model->subdivisions->city, // set the initial display text
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => '/admin/subdivisions/search-subdivisions',
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ]);
    ?>

    <?= $form->field($model, 'employees_id')->widget(Select2::classname(), [
        'initValueText' => $model->employees->name . ' ' . $model->employees->subdivisions->name . ' ' . $model->employees->subdivisions->city, // set the initial display text
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => '/admin/employees/search-employees',
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ],
    ]);
    ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <? $model->image = 'https://hh.ua/employer-logo/1819315.jpeg'; ?>
    <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'manufacture')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'installation_date')->widget(DatePicker::classname(), [
        'pluginOptions' => [
            'format' => 'dd-mm-yyyy',
            'endDate' => date('d-m-Y'),
        ],
    ]);

    ?>

    <?= $form->field($model, 'last_service_date')->widget(DatePicker::classname(), [
        'pluginOptions' => [
            'format' => 'dd-mm-yyyy',
            'endDate' => date('d-m-Y'),
        ],
    ]);

    ?>
    <? $model->status = empty($model->status)? Terminals::DEFAULT_STATUS: $model->status ?>
    <?= $form->field($model, 'status')->widget(Select2::classname(), [
        'initValueText' => in_array($model->status, Yii::$app->params['terminal_status']) ? Yii::$app->params['terminal_status'][$model->status] : '',
        'data' => Yii::$app->params['terminal_status'],
        'options' => ['placeholder' => 'Select a status.'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
