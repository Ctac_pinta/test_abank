SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `subdivisions_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employees_idx` (`id`),
  KEY `employees_name_idx` (`name`),
  KEY `subdivisions_id` (`subdivisions_id`),
  CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`subdivisions_id`) REFERENCES `subdivisions` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `employees` (`id`, `name`, `subdivisions_id`) VALUES
(1,	'работник_1',	1),
(2,	'работник_2',	2),
(3,	'работник_3',	2),
(4,	'работник_4',	3);

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base',	1535532002),
('m180829_073521_create_database',	1535532008),
('m180829_084634_add_admin_user',	1535533032);

DROP TABLE IF EXISTS `regional_centers`;
CREATE TABLE `regional_centers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `regional_centers_idx` (`id`),
  KEY `regional_centers_name_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `subdivisions`;
CREATE TABLE `subdivisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(4) NOT NULL,
  `city` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `subdivisions_idx` (`id`),
  KEY `subdivisions_name_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `subdivisions` (`id`, `name`, `city`) VALUES
(1,	'ABDP',	'Днепр'),
(2,	'ABKV',	'Киев'),
(3,	'ABLV',	'Львов'),
(4,	'ABZP',	'Запорожье ');

DROP TABLE IF EXISTS `terminals`;
CREATE TABLE `terminals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subdivisions_id` int(11) DEFAULT NULL,
  `employees_id` int(11) DEFAULT NULL,
  `code` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacture` varchar(255) DEFAULT NULL,
  `installation_date` int(11) DEFAULT NULL,
  `last_service_date` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `terminals_idx` (`id`),
  KEY `terminals_code_idx` (`code`),
  KEY `fk_terminals_subdivisions_id` (`subdivisions_id`),
  KEY `fk_terminals_employees_id` (`employees_id`),
  CONSTRAINT `fk_terminals_employees_id` FOREIGN KEY (`employees_id`) REFERENCES `employees` (`id`) ON DELETE SET NULL,
  CONSTRAINT `fk_terminals_subdivisions_id` FOREIGN KEY (`subdivisions_id`) REFERENCES `subdivisions` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

INSERT INTO `terminals` (`id`, `subdivisions_id`, `employees_id`, `code`, `image`, `manufacture`, `installation_date`, `last_service_date`, `status`) VALUES
(1,	1,	1,	'ABDP-1',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(3,	2,	1,	'ABDP-2',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(4,	2,	1,	'ABDP-3',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(5,	3,	1,	'ABDP-4',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(6,	2,	1,	'ABDP-5',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(7,	4,	1,	'ABDP-6',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(8,	2,	3,	'ABDP-7',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(9,	2,	3,	'ABDP-8',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(10,	2,	3,	'ABDP-9',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(11,	2,	3,	'ABDP-10',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(12,	2,	3,	'ABDP-11',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(13,	2,	3,	'ABDP-12',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(14,	2,	3,	'ABDP-13',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(15,	2,	3,	'ABDP-14',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(16,	2,	3,	'ABDP-15',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(17,	2,	3,	'ABDP-16',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(18,	2,	3,	'ABDP-17',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(19,	2,	3,	'ABDP-18',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(20,	2,	3,	'ABDP-19',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(21,	2,	3,	'ABDP-20',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(22,	2,	3,	'ABDP-21',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(23,	2,	3,	'ABDP-22',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(24,	2,	3,	'ABDP-23',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(25,	2,	3,	'ABDP-24',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(26,	2,	3,	'ABDP-25',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(27,	2,	3,	'ABDP-26',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(28,	2,	3,	'ABDP-27',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(29,	2,	3,	'ABDP-28',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(30,	2,	3,	'ABDP-29',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(31,	2,	3,	'ABDP-30',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(32,	2,	3,	'ABDP-31',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(33,	2,	3,	'ABDP-32',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(34,	2,	3,	'ABDP-33',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(35,	2,	3,	'ABDP-34',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(36,	2,	3,	'ABDP-35',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(37,	2,	3,	'ABDP-36',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(38,	2,	3,	'ABDP-37',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(39,	2,	3,	'ABDP-38',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(40,	2,	3,	'ABDP-39',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(41,	2,	3,	'ABDP-40',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(42,	2,	3,	'ABDP-41',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(43,	2,	3,	'ABDP-42',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(44,	2,	3,	'ABDP-43',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(45,	2,	3,	'ABDP-44',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(46,	2,	3,	'ABDP-45',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(47,	2,	3,	'ABDP-46',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(48,	2,	3,	'ABDP-47',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(49,	2,	3,	'ABDP-48',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(50,	2,	3,	'ABDP-49',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(51,	2,	3,	'ABDP-50',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(52,	2,	3,	'ABDP-51',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(53,	2,	3,	'ABDP-52',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(54,	2,	3,	'ABDP-53',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(55,	2,	3,	'ABDP-54',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(56,	2,	3,	'ABDP-55',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(57,	2,	3,	'ABDP-56',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(58,	2,	3,	'ABDP-57',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(59,	2,	3,	'ABDP-58',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(60,	2,	3,	'ABDP-59',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(61,	2,	3,	'ABDP-60',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(62,	2,	3,	'ABDP-61',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(63,	2,	3,	'ABDP-62',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(64,	2,	3,	'ABDP-63',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(65,	2,	3,	'ABDP-64',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(66,	2,	3,	'ABDP-65',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(67,	2,	3,	'ABDP-66',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(68,	2,	3,	'ABDP-67',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(69,	2,	3,	'ABDP-68',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(70,	2,	3,	'ABDP-69',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(71,	2,	3,	'ABDP-70',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(72,	2,	3,	'ABDP-71',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(73,	2,	3,	'ABDP-72',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(74,	2,	3,	'ABDP-73',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(75,	2,	3,	'ABDP-74',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(76,	2,	3,	'ABDP-75',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(77,	2,	3,	'ABDP-76',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(78,	2,	3,	'ABDP-77',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(79,	2,	3,	'ABDP-78',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(80,	2,	3,	'ABDP-79',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(81,	2,	3,	'ABDP-80',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(82,	2,	3,	'ABDP-81',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(83,	2,	3,	'ABDP-82',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(84,	2,	3,	'ABDP-83',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(85,	2,	3,	'ABDP-84',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(86,	2,	3,	'ABDP-85',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(87,	2,	3,	'ABDP-86',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(88,	2,	3,	'ABDP-87',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(89,	2,	3,	'ABDP-88',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(90,	2,	3,	'ABDP-89',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(91,	2,	3,	'ABDP-90',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(92,	2,	3,	'ABDP-91',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(93,	2,	3,	'ABDP-92',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(94,	2,	3,	'ABDP-93',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(95,	2,	3,	'ABDP-94',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(96,	2,	3,	'ABDP-95',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(97,	2,	3,	'ABDP-96',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1),
(98,	2,	3,	'ABDP-97',	'https://hh.ua/employer-logo/1819315.jpeg',	'Производитель',	1533081600,	1533081600,	1);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` text,
  `authKey` varchar(255) DEFAULT NULL,
  `accessToken` varchar(255) DEFAULT NULL,
  `loginTryCount` int(11) DEFAULT '0',
  `lastLogin` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `users_idx` (`id`),
  KEY `users_username_idx` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `username`, `password`, `authKey`, `accessToken`, `loginTryCount`, `lastLogin`) VALUES
(1,	'admin',	'$2y$13$IzPzE1PK2.O2iKWLW92A/.PkXmkLu6leMqFine1zyn.KYep1HHutK',	'21232f297a57a5a743894a0e4a801fc3',	'21232f297a57a5a743894a0e4a801fc3',	0,	0);

-- 2018-08-29 16:50:34
