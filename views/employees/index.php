<?php

use \yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $models \app\models\Employees */
/* @var $pages \yii\data\Pagination */
/* @var $filter_form \app\models\EmployeesFilterForm */

$this->title = 'Ответственые работники';

?>
<div class="site-index">
    <div><a href="/admin">ADMIN PANEL</a></div>
    <div class="row">
        <h3><?= $this->title ?></h3>

        <?php $form = ActiveForm::begin([
            'action'=>'/employees',
            'method' => 'get',
            'id' => 'filter-form',
            'fieldConfig' => [
                'template' => "<div class='col-md-12'>{label}</div><div class='row col-md-9'>\n<div class=\"col-md-12\">{input}</div>\n<div class=\"col-md-12\">{error}</div>\n</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]); ?>

        <? $filter_form->subdivisions_id = !empty($subdivisions) ? $subdivisions->id : '' ?>
        <?= $form->field($filter_form, 'subdivisions_id')->widget(Select2::classname(), [
            'initValueText' => !empty($subdivisions) ? $subdivisions->name . ' ' . $subdivisions->city : '', // set the initial display text
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                ],
                'ajax' => [
                    'url' => '/subdivisions/search-subdivisions',
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
        ]); ?>


        <div class="col-md-2">
            <?= Html::submitButton('Найти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>


        <?php ActiveForm::end(); ?>
        <table class="table table-striped">
            <thead>
            <th>ID</th>
            <th>Имя</th>
            <th>Подраздиление</th>
            <th>Количество терминалов</th>
            </thead>
            <tbody>

            <? foreach ($models as $v) { ?>
                <? /**
                 * @var \app\models\Employees $v
                 */
                ?>
                <tr>
                    <td><?= $v->id ?></td>
                    <td>
                        <?= $v->name ?>
                    </td>
                    <td>
                        <?= $v->subdivisions->name ?> - <?= $v->subdivisions->city ?>
                    </td>
                    <td>
                        <? foreach ($v->getSubdivisionsWithTerminalCount() as $t) { ?>
                            <div><?= $t['name'] ?> - <?= $t['c'] ?></div>
                        <? } ?>
                    </td>
                </tr>
            <? } ?>
            </tbody>
        </table>
    </div>

    <div class="text-center">
        <?= LinkPager::widget([
            'pagination' => $pages,
        ]); ?>
    </div>
</div>
