<?php

use \yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $models \app\models\Terminals */
/* @var $pages \yii\data\Pagination */

$this->title = 'Тестовое задание';

?>
<div class="site-index">
    <div><a href="/admin">ADMIN PANEL</a></div>
    <div class="row">
        <h3>Терминал <?= $models->code ?></h3>
        <? if (!empty($models->image)) { ?>
            <div class="img">
                <img src="<?= $models->image ?>" height="100" width="100">
            </div>
        <? } ?>
        <div>Код: <strong><?= $models->code ?></strong></div>
        <div>Произвадитель: <strong><?= $models->manufacture ?></strong></div>
        <div>Дата установки: <strong><?= $models->installation_date ?></strong></div>
        <div>Дата последнего обслужиывания: <strong><?= $models->last_service_date ?></strong></div>
        <div>Статус: <strong><?= isset(Yii::$app->params['terminal_status'][$models->status])?Yii::$app->params['terminal_status'][$models->status]:'' ?></strong></div>
    </div>
</div>
