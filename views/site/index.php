<?php

use \yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $models \app\models\Terminals */
/* @var $pages \yii\data\Pagination */

$this->title = 'Тестовое задание';

?>
<div class="site-index">
    <div><a href="/admin">ADMIN PANEL</a></div>
    <div class="row">
        <h3>Терминалы</h3>
        <table class="table table-striped">
            <thead>
            <th>ID</th>
            <th>Изображение</th>
            <th>Код</th>
            </thead>
            <tbody>

            <? foreach ($models as $v) { ?>
                <? /**
                 * @var \app\models\Terminals $v
                 */
                ?>
                <tr>
                    <td><?= $v->id ?></td>
                    <td>
                        <? if (!empty($v->image)) { ?>
                            <div class="img">
                                <img src="<?= $v->image ?>" height="50" width="50">
                            </div>
                        <? } ?>
                    </td>

                    <td class="code">
                        <a href="/site/view?id=<?= $v->id ?>">
                            <?= $v->code ?>
                        </a>
                    </td>
                </tr>
            <? } ?>
            </tbody>
        </table>
    </div>

    <div class="text-center">
        <?= LinkPager::widget([
            'pagination' => $pages,
        ]); ?>
    </div>
</div>
