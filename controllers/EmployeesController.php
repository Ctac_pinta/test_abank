<?php

namespace app\controllers;

use app\models\EmployeesFilterForm;
use app\models\Subdivisions;
use app\models\Terminals;
use app\modules\admin\models\Employees;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class EmployeesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'logout'],
                'rules' => [
                    [
                        'actions' => ['index', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * action Index.
     *
     * @return string
     */
    public function actionIndex()
    {
        $filter_form = new EmployeesFilterForm(); //model for form
        $query = Employees::find()->from('employees e'); //select Employees
        $subdivisions = [];
        if ($filter_form->load(Yii::$app->request->get())) { //if form is valid
            $query->where(['subdivisions_id' => Yii::$app->request->get('EmployeesFilterForm')['subdivisions_id']]);
            $subdivisions = Subdivisions::findOne(['id' => Yii::$app->request->get('EmployeesFilterForm')['subdivisions_id']]);
        }
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]); // pagination
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'models' => $models,
            'pages' => $pages,
            'filter_form' => $filter_form,
            'subdivisions' => $subdivisions
        ]);
    }


}
