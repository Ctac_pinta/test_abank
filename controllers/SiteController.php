<?php

namespace app\controllers;

use app\models\Terminals;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'logout'],
                'rules' => [
                    [
                        'actions' => ['index', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * action Index.
     *
     * @return string
     */
    public function actionIndex()
    {

        $query = Terminals::find(); //select Terminals
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]); //pagination
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'models' => $models,
            'pages' => $pages
        ]);
    }

    /**
     * action Logout
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goBack('/');
    }

    /**
     * action view terminals
     *
     * @param $id
     * @return string
     */

    public function actionView($id)
    {

        $models = Terminals::findOne(['id'=>$id]); //select terminals

        return $this->render('view', [
            'models' => $models,
        ]);
    }


}
