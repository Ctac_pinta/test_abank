<?php

namespace app\controllers;

use app\models\LoginUserForm;
use Yii;
use yii\web\Controller;
use yii\web\Response;


class LoginController extends Controller
{
    /**
     * action Login
     *
     * @return Response|string
     */
    public function actionIndex()
    {
        /**
         * model for login form
         */
        $model = new LoginUserForm(); //login user form
        /**
         * auth user
         */
        if ($model->load(Yii::$app->request->post()) && $model->login()) { // login user
            return $this->goBack('/admin');
        }

        $model->password = '';
        /**
         * render template
         */
        return $this->render('login', [
            'model' => $model,
        ]);
    }
}