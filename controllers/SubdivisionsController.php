<?php

namespace app\controllers;

use app\models\Subdivisions;
use app\models\Terminals;
use app\modules\admin\models\Employees;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class SubdivisionsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'logout'],
                'rules' => [
                    [
                        'actions' => ['index', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * return Subdivisions for select2
     *
     * @param null $q
     * @param null $id
     * @return array
     */
    public function actionSearchSubdivisions($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $data = Subdivisions::find()
                ->select(['id, CONCAT(`name`," ",`city`) AS text'])
                ->where(['like', 'LOWER(`name`)', (strtolower($q))])
                ->asArray()
                ->all(); //select Subdivisions
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Subdivisions::findOne(['id' => $id])->name];
        }
        return $out;
    }

}
